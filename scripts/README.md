# Python scripts for pre- and post-processing of EMMIVOX simulations

Here you will find some python scripts useful to setup and analyze EMMIVOX simulations. For info about their syntax, just type:

`python script.py -h`

Brief overview:
 * `align-VOXELS.py`: align two PDBs based on an atom selections, dump the transformation to file, apply transformation to a cryo-EM map, write transformed cryo-EM map to file;
 * `align-PDBs.py`: transform one PDB based on the inverse transformation read from file;
 * `align-XTC.py`: transform a trajectory in XTC format based on the inverse transformation read from file;
 * `cryo-EM_preprocess.py`: filter correlated voxels of a cryo-EM map, calculate error map from two half maps, zone the map close to the input model, write filtered map in PLUMED format to file;
 * `cryo-EM_validate.py`: calculate model/map fit (CCmask like) from one PDB, two PDBs, or two PDBs and a trajectory (average map);
 * `add-BFACT.py`: read a PDB file and a EMMIStatus file (with Bfactor) and create a new PDB file with Bfactor column;
 * `make_ndx.py` and `make_XTC_ndx.py`: create GROMACS index files with selection of atoms.

## **Software installation**

Let's create a conda environment to install all the python libraries needed to run pre- and post-processing tools:

* `conda create --name masterclass-22-17`

* `conda activate masterclass-22-17`

* `conda install mrcfile mdanalysis biopython -c conda-forge`

   Make sure you are installing MDAnalysis version >= 2.0.0.

* In this environment, you also need to install `pytorch` using the instructions available [here](https://pytorch.org/get-started/locally/).
  Make sure you select a version compatible with the Cuda version installed on your machine or alternatively the CPU version.

  I have installed version 1.13.0 with Cuda 11.6 using:

  `conda install pytorch torchvision torchaudio pytorch-cuda=11.6 -c pytorch -c nvidia`

  For CPU only:

  `conda install pytorch torchvision torchaudio cpuonly -c pytorch`


