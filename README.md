# A Bayesian approach to integrate cryo-EM data into MD simulations with PLUMED 

This repository is organized in the following directories:
* `scripts`: python scripts used for preprocessing and analysis of EMMIVOX simulations
* `tutorials`: two tutorials for single-structure and ensemble refinement with EMMIVOX 

## **Hardware requirements**

To complete this tutorial you need:

 * A Linux workstation with a multi-core CPU (6-8 cores should be enough);
 * For ensemble modelling, access to a Linux cluster is preferable;
 * **optional** (but *highly recommended*): a GPU to accelerate both the Molecular Dynamics simulation and the calculation of the cryo-EM restraint.

## **Software requirements**

 Make sure you have installed:

 * A modern C++ compiler that supports OpenMP parallelization and C++14. I am currently using [gcc](https://gcc.gnu.org) v.9.3.1;
 * MPI compilers. I am currently using [Open MPI](https://www.open-mpi.org) v.3.1.6;
 * [Cuda](https://developer.nvidia.com/cuda-toolkit), **optional**, but needed if you want to use your GPU to accelerate both [GROMACS](https://www.gromacs.org) and [PLUMED](https://www.plumed.org). I am currenly using v.11.6. The exact version depends a bit on how old your GPU is. I am currently using an NVIDIA GeForce RTX 3080;
 * [LibTorch](https://pytorch.org/get-started/locally/). I am currenly using v.1.13.0, but it should work with older versions starting from 1.8.0.
   Make sure you download the C++ version for Linux (i.e., LibTorch, not pytorch). If you have a GPU, please download a LibTorch version supported by the Cuda installed on your system, otherwise download the CPU version of LibTorch. Make sure you add the path to LibTorch root directory to `LD_LIBRARY_PATH` with:

   `export LD_LIBRARY_PATH=/path_to_libtorch/lib:$LD_LIBRARY_PATH`

 * [Conda](https://www.anaconda.com) to install the python libraries needed by the pre- and post-processing scripts. Have a look [here](https://gitlab.pasteur.fr/mbonomi/masterclass-22-17/-/blob/master/scripts/README.md) for more info about the libraries that you need to install with Conda;
 * [Phenix](https://phenix-online.org/documentation/index.html) (any recent version) to validate our refined model as well as the deposited PDB; 
 * UCSF [Chimera](https://www.cgl.ucsf.edu/chimera/download.html) or [ChimeraX](https://www.cgl.ucsf.edu/chimerax/) to visualize structural models and cryo-EM density maps.
 * [GROMACS](https://www.gromacs.org) and [PLUMED](https://www.plumed.org): see installation instructions below.

## **PLUMED installation**

### 1. Getting PLUMED

Clone this special version of [PLUMED](https://www.plumed.org) from Pasteur GitLab:

`git clone https://gitlab.pasteur.fr/mbonomi/plumed-EMMIVox.git`

If you do not have `git` installed, you can download a zip file of the repository [here](https://gitlab.pasteur.fr/mbonomi/plumed-EMMIVox/-/archive/master/plumed-EMMIVox-master.zip).

### 2. Configuring and hacking the `Makefile.conf`
 
First we need to configure PLUMED with:

`./configure`

Now we need to manually add to `Makefile.conf` the location of the libTorch library (plus a couple of minor edits). We are working to
make `configure` do this automatically, but we are not there yet. These are the modifications you need to do:
   * Substitute `-std=c++11` with `-std=c++14`;
   * Add `-D_GLIBCXX_USE_CXX14_ABI=1` to `CFLAGS`, `CXXFLAGS`, `CXXFLAGS_NOOPENMP`, and `LD`;
   * Add `-I/path_to_libtorch/include/torch/csrc/api/include/ -I/path_to_libtorch/include -I/path_to_libtorch/include/torch` to `CPPFLAGS` and `LD`;
   * Add `-L/path_to_libtorch/lib -ltorch -lc10 -lc10_cuda -ltorch_cpu -ltorch_cuda` to `DYNAMIC_LIBS`
   * Add `-Wl,-rpath,/path_to_libtorch/lib` to `DYNAMIC_LIBS` and `LD`
   * Make sure you are compiling with the `-O3` optimization flag in `CFLAGS`, `CXXFLAGS`, `CXXFLAGS_NOOPENMP`, and `LD`.

where `/path_to_libtorch/` is the root directory in which LibTorch is installed.

**Note**: if you downloaded the CPU version of LibTorch, please remove `-lc10_cuda` and `-ltorch_cuda`, as these libraries are not available without Cuda.

### 3. Compiling

Just type:

`make -j 16`

Here I am compiling in parallel using 16 CPU cores. You might want to change this number depending on the number of cores available on your machine.
After succesfull compilation, you will need to source the `sourceme.sh` file to have PLUMED available in your shell, wherever you are. 

`source /path_to_plumed/sourceme.sh`

## **GROMACS installation**

### 1. Getting GROMACS

You can download GROMACS v.2021.5 [here](https://manual.gromacs.org/documentation/2021.5/download.html).

```
wget https://ftp.gromacs.org/gromacs/gromacs-2021.5.tar.gz
tar xzvf gromacs-2021.5.tar.gz
cd gromacs-2021.5
```

### 2. Patching with PLUMED

Go to the root directory of GROMACS and patch with PLUMED:

`plumed patch -p --shared`

and select `gromacs-2021.5`. 

### 3. Configuring and compiling

Follow [GROMACS](https://www.gromacs.org) instructions to compile the code. This usually means creating a `build` directory in the GROMACS 
root directory and run `cmake` from within. 

`mkdir build; cd build`

* If you have a GPU and Cuda installed, you need to specify the directory 
  where Cuda is installed (`-DCUDA_TOOLKIT_ROOT_DIR`) and configure with:

  `cmake ../ -DCUDA_TOOLKIT_ROOT_DIR=/path_to_cuda/ -DGMX_GPU=CUDA -DCMAKE_INSTALL_PREFIX=/path_to_gromacs/gromacs-2021.5-plumed -DGMX_OPENMP=ON -DGMX_MPI=ON -DGMX_THREAD_MPI=OFF -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=mpicc -DGMX_BUILD_OWN_FFTW=ON`

* Otherwise, you can configure with:

  `cmake ../ -DGMX_GPU=OFF -DCMAKE_INSTALL_PREFIX=/path_to_gromacs/gromacs-2021.5-plumed -DGMX_OPENMP=ON -DGMX_MPI=ON -DGMX_THREAD_MPI=OFF -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=mpicc -DGMX_BUILD_OWN_FFTW=ON`

In both cases, you need to specify where you want to install GROMACS with `-DCMAKE_INSTALL_PREFIX`.

You can now compile and install GROMACS with:

`make install -j 16`

Here I am compiling in parallel using 16 CPU cores. You might want to change this number depending on the number of cores available on your machine. 

### 4. You are good to go

You will need to source this file in order to have GROMACS available in your shell, wherever you are:

`source /path_to_gromacs/gromacs-2021.5-plumed/bin/GMXRC`

To check that you have properly compiled GROMACS+PLUMED, please type:

`gmx_mpi mdrun -h`

and verify that the option `-plumed` is available.

## **Credits and contact**

This material has been prepared by Samuel Hoff and Max Bonomi (Institut Pasteur, France).
For any technical question, please write to mbonomi_at_pasteur.fr.
