# EMMIVOX tutorials
<p align="center">
  <img src="tau.png" width="400">
</p>

This directory contains tutorials for single-structure refinement (`1-refinement`) and ensemble modelling (`2-ensemble`) with EMMIVOX.
The system that we are going to simulate is a Tau filament resolved at 1.9 Angstrom by the Scheres group
 (PDB 7P6A, EMD 13223). The system is composed by 5 identical chains, for a total of 8335 solute atoms.
We will simulate the system using the CHARMM36m force field in explicit TIP3P water, for a total of 40958 atoms.
For more information about this system, please have a look at this beautiful paper:

[Shi, Y., Zhang, W., Yang, Y. et al. Structure-based classification of tauopathies. Nature 598, 359–363 (2021)](https://doi.org/10.1038/s41586-021-03911-7) 

**Note**: To do ensemble modelling you need to first complete the single-structure refinement tutorial.
