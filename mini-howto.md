# Mini howto

I am assuming that you have are using an Ubuntu 20.04 LTS linux distribution, where you have administrative (root/sudo) privileges
and running bash (default shell).
This initial version assumes also that you have a Nvidia gpu `[NVidia GPU only]`. Later version would add `[CPU only]` directions.

## prerequisites

### Install ubuntu provided packages
```
sudo apt-get update
sudo apt-get install  \
	build-essential libtorch3-dev libopenmpi-dev \
	tmux screen lftp wget curl vim htop \
	unzip git libfftw3-dev cmake \
	libzip-dev python3 python3-dev libzip-dev  libgsl-dev \
	hwloc
```
### [NVidia GPU only] Install CUDA (version 11.6)

```
cd
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-keyring_1.0-1_all.deb 
sudo dpkg -i cuda-keyring_1.0-1_all.deb
sudo apt-get update 
sudo apt-get -y install \
	cuda-minimal-build-11-6 \
	cuda-nvprof-11-6  libcufft-dev-11-6 

```
## adapting Max directions with the following assumptions

This will **add/replace** the following folders in your $HOME: `libtorch`, `plumed-EMMIVox`, `gromacs-2021.5` and `gromacs-2021.5-plumed`.
If you are not sure, DO NOT blindly cut/paste these lines!

### Installing `libtorch` in your `$HOME`
```
wget https://download.pytorch.org/libtorch/cu116/libtorch-cxx11-abi-shared-with-deps-1.13.0%2Bcu116.zip 
unzip -d $HOME libtorch-cxx11-abi-shared-with-deps-1.13.0+cu116.zip 
/bin/rm libtorch-cxx11-abi-shared-with-deps-1.13.0+cu116.zip
```
### Installing `plumed-EMMIVox` in your `$HOME`

```
cd
git clone https://gitlab.pasteur.fr/mbonomi/plumed-EMMIVox.git 
cd plumed-EMMIVox/
export HWLOC_COMPONENTS=-gl # workaround for ubuntu issue
./configure
curl https://gitlab.pasteur.fr/tru/masterclass-22-17/-/raw/master/Makefile.conf.patch-libtorch_in_HOME| patch -i -
make -j 4
```

### [NVidia GPU only] Installing `gromacs-2021.5` and `gromacs-2021.5-plumed` in your `$HOME`
```
cd
curl https://ftp.gromacs.org/gromacs/gromacs-2021.5.tar.gz| tar xzvf -
cd gromacs-2021.5 
source ${HOME}/plumed-EMMIVox/sourceme.sh
echo "gromacs-2021.5"| plumed-patch -p --shared  
mkdir build ;
cd build && \
cmake ../ -DCUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-11.6 -DGMX_GPU=CUDA -DCMAKE_INSTALL_PREFIX=${HOME}/gromacs-2021.5-plumed -DGMX_OPENMP=ON -DGMX_MPI=ON -DGMX_THREAD_MPI=OFF -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_C_COMPILER=mpicc -DGMX_BUILD_OWN_FFTW=ON && \
make -j 4 && make install
```

## Ready to work

```
export HWLOC_COMPONENTS=-gl
source ${HOME}/plumed-EMMIVox/sourceme.sh
source ${HOME}/gromacs-2021.5-plumed/bin/GMXRC

```
